<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xl="http://www.w3.org/1999/xlink" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:r25="http://www.collegenet.com/r25" xmlns:r25fn="http://www.collegenet.com/r25/functions" xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="#all" version="2.0">

<xsl:import href="../../../common/report-styles.xsl" />
<xsl:import href="../../../common/common.xsl" />
<xsl:output method="xml" encoding="UTF-8" indent="no" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

<xsl:param name="base_url" select="''" />
<xsl:param name="session_id" select="''" />
<xsl:param name="lang" select="'en'" />
<xsl:param name="country" select="'US'" />
<xsl:param name="timezone" />

<xsl:variable name="REPORT_FILENAME" select="reverse(tokenize(replace(static-base-uri(),'/','\\'),'\\'))[1]"/>
<xsl:variable name="REPORT_TITLE" select="/r25:reports/r25:report/r25:report_name"/>
<xsl:variable name="search_url" select="concat($base_url, 'org_search.xml?query_id=', /r25:reports/r25:report/r25:report_run/r25:ac_query_id, '&amp;session_id=',$session_id)" />
<xsl:variable name="search_name" select="document($search_url)/r25:organization_search/r25:search/r25:query_name" />
<xsl:variable name="orgs_url" select="concat($base_url, /r25:reports/r25:report/r25:report_run/r25:ac_query_id/@xl:href, '&amp;session_id=',$session_id)" />
<xsl:variable name="orgs" select="r25fn:document($orgs_url)/r25:organizations/r25:organization" />

<xsl:template match="/">
  <html><xsl:attribute name="xml:lang" select="$lang"/>
  <head>
    <title><xsl:value-of select="$REPORT_TITLE"/></title>
    <style type="text/css" media="print">
      <xsl:call-template name="CommonStyles"/>
      @page report {
      size: portrait;
      }
      #<xsl:value-of select="$REPORT"/> .Footer > div.ReportSearch:before { content: "<xsl:value-of select="r25fn:text('Organization Search')"/>: <xsl:value-of select="$search_name"/>"; }
      #<xsl:value-of select="$REPORT"/> .PadRows > td { padding-top: 0.3em; }						
      #<xsl:value-of select="$REPORT"/> .Data { font-size: 0.7em; }
    </style>
  </head>
  <body class="Local"><xsl:attribute name="id" select="$REPORT"/>
  <div class="Header"><div class="TanHeaderPort"/><div class="ReportTitleWhite"><xsl:value-of select="$REPORT_TITLE"/></div></div>
  <xsl:call-template name="FooterSearch"/>
  <div class="Data">
    <table class="Grid">
      <col width="2*"/>
      <col width="3*"/>
      <col width="1.5*"/>
      <thead>
        <tr>
          <th><xsl:value-of select="r25fn:text('id')"/></th>
          <th><xsl:value-of select="r25fn:text('Name')"/></th>
          <th><xsl:value-of select="r25fn:text('Last Mod')"/></th>
        </tr>
      </thead>
      <tbody>
        <tr class="PadTop"><td/></tr>
        <xsl:choose>
          <xsl:when test="count($orgs) = 0">
            <tr>
              <td colspan="3" class="NoData"><xsl:value-of select="r25fn:text('No organizations found')"/></td>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="$orgs">
              <xsl:sort select="r25:organization_name"/>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </tbody>
    </table>
  
  </div>
  </body>
  </html>
</xsl:template>

<xsl:template match="r25:organization">
  <tr class="PadRows"><td/></tr>
  <tr class="Container">
    <xsl:if test="position() mod 2 = 0">
      <xsl:attribute name="style" select="concat('background-color:', $BACKGROUND_COLOR)"/>
    </xsl:if>		
    <td><xsl:value-of select="r25:organization_id" /></td>
    <td><xsl:value-of select="r25:organization_name" /></td>
    <td><xsl:value-of select="r25fn:format-dateTime(xs:dateTime(r25:last_mod_dt))" /></td>
  </tr>
</xsl:template>

</xsl:stylesheet>
